using UnityEngine;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth.Utility {

    public class Timer {

        /////////////////////////////////////////////////////////////////
        // P U B L I C   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        public float TimerDelay { get; set; }
        public float CurrentTime { get; private set; }
        public bool IsStarted { get; private set; }
        public bool IsPaused { get; private set; }

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        public delegate void TimerAction ();
        private TimerAction timerAction;

        /////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        /////////////////////////////////////////////////////////////////

        public Timer (float timerDelay) {
            TimerDelay = timerDelay;
            CurrentTime = 0.0f;
        }

        public Timer (float timerDelay, TimerAction timerAction) {
            TimerDelay = timerDelay;
            this.timerAction = timerAction;
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public void Tick () {
            if (IsStarted) {
                CurrentTime += Time.deltaTime;

                if (!(timerAction == null || timerAction.Equals(null))) {
                    if (CurrentTime >= TimerDelay) {
                        timerAction();
                        Reset();
                    }
                }
            }
        }

        public void Start () {
            IsStarted = true;
            IsPaused = false;
        }

        public void Stop () {
            IsStarted = false;
            IsPaused = false;
            Reset();
        }

        public void Pause () {
            IsStarted = false;
            IsPaused = true;
        }

        public void Reset () {
            CurrentTime = 0.0f;
        }
    }
}
